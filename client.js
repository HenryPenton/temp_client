var ioClient = require("socket.io-client"),
	rpiDhtSensor = require("rpi-dht-sensor"),
	socket = ioClient.connect("http://192.168.1.104:4000"),
	average = [],
	i = 0,
	room = "Henry",
	dht = new rpiDhtSensor.DHT11(2);


for (var b = 0; b < 720; b++) {
	average[b]=0
}


setInterval(function() {
	var a = read();
	average[i] = a.temperature;
	i++;
	i %= 720;
	for (var c = 0, b = 0; b < average.length; b++) c += parseInt(average[b], 10);
	socket.emit("incoming", {
		room: room,
		temperature: a.temperature,
		humidity: a.humidity,
		averagepast12h: (c / average.length).toFixed(1),
		graphTemps: graphTemps(),
		graphTimes: graphTimes()
	})
}, 60000);

function read() {
	return dht.read()
};

function graphTemps(){
	var temps=[];
	for (var f = 0; f < 12; f++) {
		var q = ((i-1) - ((f)*60))
		if (q<0){
			q=720-Math.abs(q);
		}
		temps[f]=average[q]
	}
	temps.reverse();
return temps;
};
function graphTimes(){
	var d = new Date();
	var times=[];
	for (var f = 0; f < 12; f++) {
		var time = d.getHours()-(1*(11-f));
		if(time<0){
			time = 24 - Math.abs(time);
		}
		times[f] = (time +":" +(d.getMinutes()<10?'0':'')+d.getMinutes());
	}
	return times;
};
